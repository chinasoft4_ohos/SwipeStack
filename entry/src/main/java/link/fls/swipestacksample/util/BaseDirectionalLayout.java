/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package link.fls.swipestacksample.util;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

/**
 * 自定义线性布局，实现点击背景变成深色的效果
 *
 * @since 2021-08-21
 */
public class BaseDirectionalLayout extends ohos.agp.components.DirectionalLayout
        implements Component.TouchEventListener, Component.ClickedListener, Component.LongClickedListener {
    private ShapeElement mGrayShapeElement = new ShapeElement();
    private ShapeElement mWhiteShapeElement = new ShapeElement();
    private float oldContentY = 0;
    private float oldContentX = 0;
    private boolean isEnable = true;
    private boolean isNeedBack = false;

    /**
     * BaseDirectionalLayout的构造方法，在Java类中new时调用
     *
     * @param context
     */
    public BaseDirectionalLayout(Context context) {
        super(context);
        initShapeElement();
    }

    /**
     * BaseDirectionalLayout的构造方法，在xml布局中
     *
     * @param context
     * @param attrSet
     */
    public BaseDirectionalLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);
        initShapeElement();
    }

    /**
     * BaseDirectionalLayout的构造方法，在xml布局且加Style时调用
     *
     * @param context
     * @param attrSet
     * @param styleName
     */
    public BaseDirectionalLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initShapeElement();
    }

    /**
     * setEnable
     *
     * @param enable isEnable
     */
    public void setEnable(boolean enable) {
        this.isEnable = enable;
    }

    /**
     * setNeedBack
     *
     * @param isBack isBack
     */
    public void setNeedBack(boolean isBack) {
        this.isNeedBack = isBack;
    }

    private void initShapeElement() {
        setLongClickedListener(this::onLongClicked);
        setClickedListener(this::onClick);
        setTouchEventListener(this::onTouchEvent);
        mGrayShapeElement.setRgbColor(new RgbColor(FinalStaticBean.NUM_120,
                FinalStaticBean.NUM_126,
                FinalStaticBean.NUM_130));
        mWhiteShapeElement.setRgbColor(new RgbColor(FinalStaticBean.NUM_255,
                FinalStaticBean.NUM_255,
                FinalStaticBean.NUM_255));
    }

    @Override
    public void onClick(Component component) {

    }

    /**
     * onTouchEvent
     *
     * @param component  component
     * @param touchEvent touchEvent
     * @return onTouchEvent
     */
    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        int action = touchEvent.getAction();
        switch (action) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                setBackground(mGrayShapeElement);
                setAlpha(FinalStaticBean.NUM_HALF);
                oldContentX = getContentPositionX();
                oldContentY = getContentPositionY();
                break;
            case TouchEvent.POINT_MOVE:
                setBackground(mWhiteShapeElement);
                setAlpha(1);
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                if (isNeedBack && isEnable) {
                    setContentPosition(oldContentX, oldContentY);
                }
                setBackground(mWhiteShapeElement);
                setAlpha(1);
                break;
            case TouchEvent.CANCEL:
                setBackground(mWhiteShapeElement);
                setAlpha(1);
                break;
            default:
                break;
        }
        return true;
    }

    /**
     * onLongClicked
     *
     * @param component component
     */
    @Override
    public void onLongClicked(Component component) {
    }
}
