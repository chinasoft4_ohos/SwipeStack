/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package link.fls.swipestacksample.util;

/**
 * FastClickUtil
 *
 * @since 2021-08-21
 */
public class FastClickUtil {
    /**
     * 两次点击按钮之间的点击间隔不能少于1000毫秒
     */
    private static final int MIN_CLICK_DELAY_TIME =800;
    private static long lastClickTime = -1;

    /**
     * 是否为快速点击
     *
     * @return 快速点击
     */
    public static boolean isFastClick() {
        boolean isFlag;
        long curClickTime = System.currentTimeMillis();
        if (curClickTime - lastClickTime > MIN_CLICK_DELAY_TIME) {
            isFlag = false;
        } else {
            isFlag = true;
        }
        lastClickTime = curClickTime;
        return isFlag;
    }
}
