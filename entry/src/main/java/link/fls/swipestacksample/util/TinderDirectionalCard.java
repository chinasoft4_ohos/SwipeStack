/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package link.fls.swipestacksample.util;

import link.fls.swipestack.BaseSwipeCard;
import link.fls.swipestacksample.ResourceTable;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.app.Context;

/**
 * TinderDirectionalCard
 *
 * @since 2021-08-17
 */
public class TinderDirectionalCard extends BaseSwipeCard {
    private Text nameText;
    private Component accept;
    private Component reject;

    @Override
    public void showAccept() {
    }

    @Override
    public void showReject() {
    }

    @Override
    public void hideAllTips() {
        accept.setVisibility(HIDE);
        reject.setVisibility(HIDE);
    }

    /**
     * 获取中央显示文字
     *
     * @return 文字内容
     */
    @Override
    public String getText() {
        return nameText.getText();
    }

    /**
     * 构造函数
     *
     * @param context 上下文
     */
    public TinderDirectionalCard(Context context) {
        this(context, null);
    }

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param attrSet 属性
     */
    public TinderDirectionalCard(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    /**
     * 构造函数
     *
     * @param context   上下文
     * @param attrSet   属性
     * @param styleName 样式名
     */
    public TinderDirectionalCard(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        Component component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_tinder_card_view, null, true);
        component.setHeight(1280);
        component.setWidth(720);
        nameText = (Text) component.findComponentById(ResourceTable.Id_nameAgeTxt);
        accept = component.findComponentById(ResourceTable.Id_tvAccept);
        reject = component.findComponentById(ResourceTable.Id_tvReject);
        addComponent(component);
    }

    /**
     * 设置姓名
     *
     * @param text 姓名集合
     */
    public void setNameText(String text) {
        nameText.setText(text);
    }

    /**
     * getNameText
     *
     * @return getNameText
     */
    public String getNameText() {
        return nameText.getText();
    }
}
