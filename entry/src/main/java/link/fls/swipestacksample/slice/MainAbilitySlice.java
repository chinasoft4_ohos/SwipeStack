package link.fls.swipestacksample.slice;

import link.fls.swipestack.SwipeDirectionalView;
import link.fls.swipestacksample.ResourceTable;
import link.fls.swipestacksample.snackbar.SnackBar;
import link.fls.swipestacksample.util.FastClickUtil;
import link.fls.swipestacksample.util.TinderDirectionalCard;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.*;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.PopupDialog;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.WindowManager;
import ohos.utils.IntentConstants;
import ohos.utils.net.Uri;

import java.security.SecureRandom;

/**
 * MainAbilitySlice
 *
 * @since 2021-08-10
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener, SnackBar.OnVisibilityChangeListener, SwipeDirectionalView.SwipeStackListener {
    private Button mButtonLeft, mButtonRight;
    private Image mFab;
    private Image loadMore;
    private Text tvTitle;
    private int mCount = 1;
    private SwipeDirectionalView mSwipeStack;
    private Component button;
    private SnackBar stackReset;
    private SecureRandom random = new SecureRandom();
    private boolean isViewSwipedToLeft = true;
    private boolean isViewSwipedToRight = true;
    private boolean isClick = true;
    private int addCardNum;
    private int itemAllNum = 1;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(Color.getIntColor("#2E409F"));
        mSwipeStack = (SwipeDirectionalView) findComponentById(ResourceTable.Id_swipeStack);
        mButtonLeft = (Button) findComponentById(ResourceTable.Id_buttonSwipeLeft);
        mButtonRight = (Button) findComponentById(ResourceTable.Id_buttonSwipeRight);
        mFab = (Image) findComponentById(ResourceTable.Id_fabAdd);
        button = findComponentById(ResourceTable.Id_layout_button);
        mButtonLeft.setClickedListener(this);
        mButtonRight.setClickedListener(this);
        mFab.setClickedListener(this);
        mSwipeStack.setSwipeStackListener(this);
        addStack();
        addCardNum = 0;
        loadMore = (Image) findComponentById(ResourceTable.Id_iv_load_more);
        tvTitle = (Text) findComponentById(ResourceTable.Id_tv_title);
        tvTitle.setFont(Font.DEFAULT_BOLD);

        loadMore.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                PopupDialog dialog = new PopupDialog(MainAbilitySlice.this, null);
                Component parse = LayoutScatter.getInstance(MainAbilitySlice.this).parse(ResourceTable.Layout_popu_item, null, false);
                dialog.setCustomComponent(parse);
                dialog.setAlignment(LayoutAlignment.RIGHT | LayoutAlignment.TOP);
                dialog.setAutoClosable(true);
                dialog.show();
                Component settings = parse.findComponentById(ResourceTable.Id_reset);
                settings.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        short time = 3000;
                        mSwipeStack.clearComponentList();
                        mCount = 1;
                        mSwipeStack.undoLastSwipe();
                        dialog.remove();
                        stackReset = new SnackBar.Builder(MainAbilitySlice.this, button)
                                .withVisibilityChangeListener(MainAbilitySlice.this)
                                .withMessage("Stack reset")
                                .withBackgroundColorId(Color.getIntColor("#3C3F41"))
                                .withDuration(time)
                                .withTypeFace(Font.DEFAULT)
                                .show();
                        addStack();
                        itemAllNum = 1;
                        if (addCardNum > 0) {
                            for (int i = 0; i < addCardNum; i++) {
                                mSwipeStack.addView(makeCard("Fab Test"), random.nextInt(20) - 10);
                            }
                        }
                    }
                });
                parse.findComponentById(ResourceTable.Id_view).setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        initWeb(getString(ResourceTable.String_main_url_value));
                    }
                });
            }
        });
    }

    private void addStack() {
        for (int i = 0; i < 5; i++) {
            mSwipeStack.addView(makeCard("Test " + mCount++), random.nextInt(10) - 5);
        }
    }

    /**
     * TinderDirectionalCard
     *
     * @param cardName
     * @return tinderDirectionalCard
     */
    private TinderDirectionalCard makeCard(String cardName) {
        TinderDirectionalCard tinderDirectionalCard = new TinderDirectionalCard(getContext());
        tinderDirectionalCard.setNameText(cardName);
        return tinderDirectionalCard;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_buttonSwipeLeft:
                if (FastClickUtil.isFastClick() && isClick) {
                    return;
                }
                isClick = false;
                isViewSwipedToLeft = true;
                mSwipeStack.doSwipe(false);
                break;
            case ResourceTable.Id_buttonSwipeRight:
                if (FastClickUtil.isFastClick() && isClick) {
                    return;
                }
                isClick = false;
                isViewSwipedToRight = true;
                mSwipeStack.doSwipe(true);
                break;
            case ResourceTable.Id_fabAdd:
                mSwipeStack.addView(makeCard("Fab Test"), random.nextInt(20) - 10);
                addCardNum++;
                break;
            default:
                break;
        }
    }

    @Override
    public void onViewSwipedToLeft(int position, String text) {
        itemAllNum++;
        if (isViewSwipedToLeft) {
            if (!text.contains("Fab")) {
                toast(text + getString(ResourceTable.String_view_swiped_left));
            } else {
                toast("Fab Test swiped to left ");
            }
            isViewSwipedToLeft = false;
            isClick = true;
        }
    }

    @Override
    public void onViewSwipedToRight(int position, String text) {
        itemAllNum++;
        if (isViewSwipedToRight) {
            if (!text.contains("Fab")) {
                toast(text + getString(ResourceTable.String_view_swiped_right));
            } else {
                toast("Fab Test swiped to right");
            }
            isViewSwipedToRight = false;
            isClick = true;
        }
    }

    /**
     * getIntCount
     *
     * @return itemAllNum
     */
    public int getIntCount() {
        return itemAllNum;
    }

    @Override
    public void onStackEmpty() {
        toast("Card stack empty!");
    }

    /**
     * toast
     *
     * @param msg
     */
    private void toast(String msg) {
        new ToastDialog(this)
                .setText(msg)
                .show();
    }

    /**
     * initWeb
     *
     * @param urlString urlString
     */
    private void initWeb(String urlString) {
        Intent intents = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withUri(Uri.parse(urlString))
                .withAction(IntentConstants.ACTION_SEARCH)
                .build();
        intents.setOperation(operation);
        startAbility(intents);
    }


    /**
     * onShow
     *
     * @param stackSize the number of messages left to show
     */
    @Override
    public void onShow(int stackSize) {
        mFab.setMarginBottom(180);

        mButtonLeft.setVisibility(Component.HIDE);
        mButtonRight.setVisibility(Component.HIDE);
    }

    /**
     * onHide
     *
     * @param stackSize the number of messages left to show
     */
    @Override
    public void onHide(int stackSize) {
        mFab.setMarginBottom(40);
        mButtonLeft.setVisibility(Component.VISIBLE);
        mButtonRight.setVisibility(Component.VISIBLE);
    }
}
