/*
 *    Copyright 2014 MrEngineer13
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package link.fls.swipestacksample.snackbar;

import link.fls.swipestacksample.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.utils.Sequenceable;

/**
 * SnackBar
 *
 * @since 2021-08-10
 */
public class SnackBar {
    /**
     * MED_SNACK
     */
    public static final short MED_SNACK = 3500;
    private SnackContainer mSnackContainer;
    private Component mParentView;
    private OnMessageClickListener mClickListener;
    private OnVisibilityChangeListener mVisibilityChangeListener;

    /**
     * OnMessageClickListener
     */
    public interface OnMessageClickListener {
        void onMessageClick(Sequenceable token);
    }

    /**
     * OnVisibilityChangeListener
     */
    public interface OnVisibilityChangeListener {

        /**
         * Gets called when a message is shown
         *
         * @param stackSize the number of messages left to show
         */
        void onShow(int stackSize);

        /**
         * Gets called when a message is hidden
         *
         * @param stackSize the number of messages left to show
         */
        void onHide(int stackSize);
    }

    /**
     * SnackBar
     *
     * @param ability
     * @param component
     */
    public SnackBar(Ability ability, Component component) {
        Context context = ability.getContext();
        LayoutScatter scatter = LayoutScatter.getInstance(context);
        ComponentContainer container = (ComponentContainer) component.getComponentParent();
        scatter.parse(ResourceTable.Layout_sb__snack_container, container, true);
        Component v = scatter.parse(ResourceTable.Layout_sb__snack, container, false);
        init(container, v);
    }

    /**
     * SnackBar
     *
     * @param context
     * @param v
     */
    public SnackBar(Context context, Component v) {
        LayoutScatter scatter = LayoutScatter.getInstance(context);
        Component snackLayout = scatter.parse(ResourceTable.Layout_sb__snack, (ComponentContainer) v, false);
        init((ComponentContainer) v, snackLayout);
    }

    /**
     * init
     *
     * @param container
     * @param v
     */
    private void init(ComponentContainer container, Component v) {
        mSnackContainer = (SnackContainer) container.findComponentById(ResourceTable.Id_snackContainer);
        if (mSnackContainer == null) {
            mSnackContainer = new SnackContainer(container);
        }
        mParentView = v;
        Text snackBtn = (Text) v.findComponentById(ResourceTable.Id_snackButton);
        snackBtn.setClickedListener(mButtonListener);
    }

    /**
     * Builder
     */
    public static class Builder {
        private SnackBar mSnackBar;
        private Context mContext;
        private String mMessage;
        private String mActionMessage;
        private int mActionIcon = 0;
        private Sequenceable mToken;
        private short mDuration = MED_SNACK;
        private int mTextColor;
        private int mBackgroundColor;
        private int mHeight;
        private boolean mClear;
        private boolean mAnimateClear;
        private Font mTypeFace;

        /**
         * Builder
         *
         * @param ability
         * @param component
         */
        public Builder(Ability ability, Component component) {
            mContext = ability.getApplicationContext();
            mSnackBar = new SnackBar(ability, component);
        }

        /**
         * Constructs a new SnackBar
         *
         * @param context the context used to obtain resources
         * @param v       the view to inflate the SnackBar into
         */
        public Builder(Context context, Component v) {
            mContext = context;
            mSnackBar = new SnackBar(context, v);
        }

        /**
         * Sets the message to display on the SnackBar
         *
         * @param message the literal string to display
         * @return this builder
         */
        public Builder withMessage(String message) {
            mMessage = message;
            return this;
        }

        /**
         * Sets the message to display as the action message
         *
         * @param actionMessage the literal string to display
         * @return this builder
         */
        public Builder withActionMessage(String actionMessage) {
            mActionMessage = actionMessage;
            return this;
        }

        /**
         * Sets the action icon
         *
         * @param id the resource id of the icon to display
         * @return this builder
         */
        public Builder withActionIconId(int id) {
            mActionIcon = id;
            return this;
        }

        /**
         * withStyle
         *
         * @param color
         * @return this
         */
        public Builder withStyle(int color) {
            mTextColor = color;
            return this;
        }

        /**
         * The token used to restore the SnackBar state
         *
         * @param token the parcelable containing the saved SnackBar
         * @return this builder
         */
        public Builder withToken(Sequenceable token) {
            mToken = token;
            return this;
        }

        /**
         * Sets the duration to show the message
         *
         * @param duration the number of milliseconds to show the message
         * @return this builder
         */
        public Builder withDuration(Short duration) {
            mDuration = duration;
            return this;
        }

        /**
         * withBackgroundColorId
         *
         * @param colorId
         * @return this
         */
        public Builder withBackgroundColorId(int colorId) {
            mBackgroundColor = colorId;
            return this;
        }

        /**
         * Sets the height for SnackBar
         *
         * @param height the height of SnackBar
         * @return this builder
         */
        public Builder withSnackBarHeight(int height) {
            mHeight = height;
            return this;
        }

        /**
         * Sets the OnClickListener for the action button
         *
         * @param onClickListener the listener to inform of click events
         * @return this builder
         */
        public Builder withOnClickListener(OnMessageClickListener onClickListener) {
            mSnackBar.setOnClickListener(onClickListener);
            return this;
        }

        /**
         * Sets the visibilityChangeListener for the SnackBar
         *
         * @param visibilityChangeListener the listener to inform of visibility changes
         * @return this builder
         */
        public Builder withVisibilityChangeListener(OnVisibilityChangeListener visibilityChangeListener) {
            mSnackBar.setOnVisibilityChangeListener(visibilityChangeListener);
            return this;
        }

        /**
         * Clears all of the queued SnackBars, animates the message being hidden
         *
         * @return this builder
         */
        public Builder withClearQueued() {
            return withClearQueued(true);
        }

        /**
         * Clears all of the queued SnackBars
         *
         * @param animate whether or not to animate the messages being hidden
         * @return this builder
         */
        public Builder withClearQueued(boolean animate) {
            mAnimateClear = animate;
            mClear = true;
            return this;
        }

        /**
         * Sets the Typeface for the SnackBar
         *
         * @param typeFace the typeface to apply to the SnackBar
         * @return this builder
         */
        public Builder withTypeFace(Font typeFace) {
            mTypeFace = typeFace;
            return this;
        }

        /**
         * Shows the first message in the SnackBar
         *
         * @return the SnackBar
         */
        public SnackBar show() {
            Snack message = new Snack(mMessage,
                    (mActionMessage != null ? mActionMessage.toUpperCase() : null),
                    mActionIcon,
                    mToken,
                    mDuration,
                    mTextColor != 100 ? mTextColor : Color.WHITE.getValue(),
                    mBackgroundColor != 100 ? mBackgroundColor : Color.WHITE.getValue(),
                    mHeight != 0 ? mHeight : 0,
                    mTypeFace);

            if (mClear) {
                mSnackBar.clear(mAnimateClear);
            }

            mSnackBar.showMessage(message);

            return mSnackBar;
        }
    }

    /**
     * showMessage
     *
     * @param message
     */
    private void showMessage(Snack message) {
        mSnackContainer.showSnack(message, mParentView, mVisibilityChangeListener);
    }

    /**
     * Getter for the SnackBars parent view
     *
     * @return the parent view
     */
    public Component getContainerView() {
        return mParentView;
    }

    /**
     * mButtonListener
     */
    private final Component.ClickedListener mButtonListener = new Component.ClickedListener() {
        @Override
        public void onClick(Component component) {
            if (mClickListener != null && mSnackContainer.isShowing()) {
                mClickListener.onMessageClick(mSnackContainer.peek().mToken);
            }
            mSnackContainer.hide();
        }
    };

    /**
     * setOnClickListener
     *
     * @param listener
     * @return this
     */
    private SnackBar setOnClickListener(OnMessageClickListener listener) {
        mClickListener = listener;
        return this;
    }

    /**
     * setOnVisibilityChangeListener
     *
     * @param listener
     * @return this
     */
    private SnackBar setOnVisibilityChangeListener(OnVisibilityChangeListener listener) {
        mVisibilityChangeListener = listener;
        return this;
    }

    /**
     * Clears all of the queued messages
     *
     * @param animate whether or not to animate the messages being hidden
     */
    public void clear(boolean animate) {
        mSnackContainer.clearSnacks(animate);
    }

    /**
     * Clears all of the queued messages
     */
    public void clear() {
        clear(true);
    }

    /**
     * Hides all snacks
     */
    public void hide() {
        mSnackContainer.hide();
        clear();
    }

    /**
     * All snacks will be restored using the view from this Snackbar
     *
     * @param state
     */
    public void onRestoreInstanceState(Intent state) {
        mSnackContainer.restoreState(state, mParentView);
    }

    /**
     * onSaveInstanceState
     *
     * @return onSaveInstanceState
     */
    public Intent onSaveInstanceState() {
        return mSnackContainer.saveState();
    }
}
