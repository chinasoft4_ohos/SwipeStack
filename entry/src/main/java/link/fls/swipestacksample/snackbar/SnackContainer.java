/*
 *    Copyright 2014 MrEngineer13
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package link.fls.swipestacksample.snackbar;

import link.fls.swipestacksample.ResourceTable;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.utils.Sequenceable;

import java.util.LinkedList;
import java.util.Queue;

/**
 * SnackContainer
 *
 * @since 2021-08-10
 */
public class SnackContainer extends StackLayout {
    private static final int ANIMATION_DURATION = 300;
    private static final String SAVED_MSGS = "SAVED_MSGS";
    private Queue<SnackHolder> mSnacks = new LinkedList<SnackHolder>();
    private AnimatorGroup mOutAnimationSet;
    private AnimatorProperty mSlideInAnimation;
    private AnimatorProperty mSlideOutAnimation;
    private EventHandler handler;

    /**
     * SnackContainer
     *
     * @param context
     */
    public SnackContainer(Context context) {
        super(context);
        init();
    }

    /**
     * SnackContainer
     *
     * @param context
     * @param attrs
     */
    public SnackContainer(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    /**
     * SnackContainer
     *
     * @param container
     */
    public SnackContainer(ComponentContainer container) {
        super(container.getContext());
        container.addComponent(this, new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT));
        setVisibility(Component.HIDE);
        setId(ResourceTable.Id_snackContainer);
        init();
    }

    /**
     * init
     */
    private void init() {
        mSlideInAnimation = new AnimatorProperty();
        mSlideInAnimation.alpha(0.9f).moveFromX(0.0f).moveToX(0.0f).moveFromY(1.0f).moveToY(0.0f);
        mOutAnimationSet = new AnimatorGroup();
        mSlideOutAnimation = new AnimatorProperty();
        mSlideOutAnimation.alpha(0.1f).moveFromX(0.0f).moveToX(0.0f).moveFromY(0.0f).moveToY(1.0f);
        mSlideOutAnimation.setDuration(ANIMATION_DURATION);
        mSlideOutAnimation.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                mOutAnimationSet.end();
            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                removeAllComponents();

                if (!mSnacks.isEmpty()) {
                    sendOnHide(mSnacks.poll());
                }

                if (!isEmpty()) {
                    showSnack(mSnacks.peek());
                } else {
                    setVisibility(Component.HIDE);
                }
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
    }

    /**
     * isEmpty
     *
     * @return isEmpty
     */
    public boolean isEmpty() {
        return mSnacks.isEmpty();
    }

    /**
     * peek
     *
     * @return peek
     */
    public Snack peek() {
        return mSnacks.peek().snack;
    }

    /**
     * pollSnack
     *
     * @return pollSnack
     */
    public Snack pollSnack() {
        return mSnacks.poll().snack;
    }

    /**
     * clearSnacks
     *
     * @param animate
     */
    public void clearSnacks(boolean animate) {
        mSnacks.clear();
        handler = new EventHandler(EventRunner.create());
        handler.removeTask(mHideRunnable);
        if (animate) mHideRunnable.run();
    }

    /**
     * isShowing
     *
     * @return isShowing
     */
    public boolean isShowing() {
        return !mSnacks.isEmpty();
    }

    /**
     * hide
     */
    public void hide() {
        handler = new EventHandler(EventRunner.create());
        handler.removeTask(mHideRunnable);
        mHideRunnable.run();
    }

    /**
     * showSnack
     *
     * @param snack
     * @param snackView
     * @param listener
     */
    public void showSnack(Snack snack, Component snackView, SnackBar.OnVisibilityChangeListener listener) {
        showSnack(snack, snackView, listener, false);
    }

    /**
     * showSnack
     *
     * @param snack
     * @param snackView
     * @param listener
     * @param immediately
     */
    public void showSnack(Snack snack, Component snackView, SnackBar.OnVisibilityChangeListener listener, boolean immediately) {
        if (snackView.getComponentParent() != null && snackView.getComponentParent() != this) {
            snackView.getComponentParent().removeComponent(snackView);
        }
        SnackHolder holder = new SnackHolder(snack, snackView, listener);
        mSnacks.offer(holder);
        if (mSnacks.size() == 1) showSnack(holder, immediately);
    }

    /**
     * showSnack
     *
     * @param holder
     */
    private void showSnack(final SnackHolder holder) {
        showSnack(holder, false);
    }

    /**
     * showSnack
     *
     * @param holder
     * @param showImmediately
     */
    private void showSnack(final SnackHolder holder, boolean showImmediately) {
        setVisibility(Component.VISIBLE);
        sendOnShow(holder);
        addComponent(holder.snackView);
        holder.messageView.setText(holder.snack.mMessage);
        if (holder.snack.mActionMessage != null) {
            holder.button.setVisibility(Component.VISIBLE);
            holder.button.setText(holder.snack.mActionMessage);
        } else {
            holder.button.setVisibility(Component.HIDE);
        }
        holder.button.setFont(holder.snack.mTypeface);
        holder.messageView.setFont(holder.snack.mTypeface);
        Color color1 = new Color(holder.snack.mBtnTextColor);
        holder.button.setTextColor(color1);
        RgbColor rgbColor = RgbColor.fromArgbInt(holder.snack.mBackgroundColor);
        ShapeElement element = new ShapeElement();
        element.setRgbColor(rgbColor);
        holder.snackView.setBackground(element);
        if (holder.snack.mHeight > 0)
            holder.snackView.getLayoutConfig().height = this.getPxFromDp(holder.snack.mHeight);

        if (showImmediately) {
            mSlideInAnimation.setDuration(0);
        } else {
            mSlideInAnimation.setDuration(ANIMATION_DURATION);
        }
        mSlideInAnimation.setTarget(holder.snackView);
        mSlideOutAnimation.setTarget(holder.snackView);
        mSlideInAnimation.start();
        if (holder.snack.mDuration > 0) {
            handler = new EventHandler(EventRunner.getMainEventRunner());
            handler.postTask(mHideRunnable, holder.snack.mDuration);
        }
    }

    /**
     * sendOnHide
     *
     * @param snackHolder
     */
    private void sendOnHide(SnackHolder snackHolder) {
        if (snackHolder.visListener != null) {
            snackHolder.visListener.onHide(mSnacks.size());
        }
    }

    /**
     * sendOnShow
     *
     * @param snackHolder
     */
    private void sendOnShow(SnackHolder snackHolder) {
        if (snackHolder.visListener != null) {
            snackHolder.visListener.onShow(mSnacks.size());
        }
    }

    /**
     * mHideRunnable
     */
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            if (Component.VISIBLE == getVisibility()) {
                mSlideOutAnimation.start();
            }
        }
    };

    /**
     * restoreState
     *
     * @param state
     * @param v
     */
    public void restoreState(Intent state, Component v) {
        Sequenceable[] messages = state.getParcelableArrayParam(SAVED_MSGS);
        boolean showImmediately = true;

        for (Sequenceable message : messages) {
            showSnack((Snack) message, v, null, showImmediately);
            showImmediately = false;
        }
    }

    /**
     * saveState
     *
     * @return saveState
     */
    public Intent saveState() {
        Intent outState = new Intent();
        final int count = mSnacks.size();
        final Snack[] snacks = new Snack[count];
        int i = 0;
        for (SnackHolder holder : mSnacks) {
            snacks[i++] = holder.snack;
        }
        outState.setParam(SAVED_MSGS, snacks);
        return outState;
    }

    /**
     * SnackHolder
     */
    private static class SnackHolder {
        final Component snackView;
        final Text messageView;
        final Text button;
        final Snack snack;
        final SnackBar.OnVisibilityChangeListener visListener;

        private SnackHolder(Snack snack, Component snackView, SnackBar.OnVisibilityChangeListener listener) {
            this.snackView = snackView;
            button = (Text) snackView.findComponentById(ResourceTable.Id_snackButton);
            messageView = (Text) snackView.findComponentById(ResourceTable.Id_snackMessage);

            this.snack = snack;
            visListener = listener;
        }
    }

    /**
     * getPxFromDp
     *
     * @param dp
     * @return getPxFromDp
     */
    private int getPxFromDp(int dp) {
        int pxConverter = 2;
        int px = pxConverter * dp;
        return px;
    }
}
