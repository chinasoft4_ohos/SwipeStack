/*
 *    Copyright 2014 MrEngineer13
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package link.fls.swipestacksample.snackbar;

import ohos.agp.text.Font;
import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

/**
 * Snack
 *
 * @since 2021-08-10
 */
public class Snack implements Sequenceable {
    /**
     * mMessage
     */
    public final String mMessage;
    /**
     * mActionMessage
     */
    public final String mActionMessage;
    /**
     * mActionIcon
     */
    private final int mActionIcon;
    /**
     * mToken
     */
    public final Sequenceable mToken;
    /**
     * mDuration
     */
    public final short mDuration;
    /**
     * mBtnTextColor
     */
    public final int mBtnTextColor;
    /**
     * mBackgroundColor
     */
    public final int mBackgroundColor;
    /**
     * mHeight
     */
    public final int mHeight;
    /**
     * mTypeface
     */
    public Font mTypeface;

    /**
     * Snack
     *
     * @param message
     * @param actionMessage
     * @param actionIcon
     * @param token
     * @param duration
     * @param textColor
     * @param backgroundColor
     * @param height
     * @param typeFace
     */
    public Snack(String message, String actionMessage, int actionIcon,
                 Sequenceable token, short duration, int textColor, int backgroundColor, int height, Font typeFace) {
        mMessage = message;
        mActionMessage = actionMessage;
        mActionIcon = actionIcon;
        mToken = token;
        mDuration = duration;
        mBtnTextColor = textColor;
        mBackgroundColor = backgroundColor;
        mHeight = height;
        mTypeface = typeFace;
    }

    /**
     * Snack
     *
     * @param p
     */
    public Snack(Parcel p) {
        mMessage = p.readString();
        mActionMessage = p.readString();
        mActionIcon = p.readInt();
        mToken = p.readSequenceableList(Sequenceable.class).get(0);
        mDuration = (short) p.readInt();
        mBtnTextColor = p.readInt();
        mBackgroundColor = p.readInt();
        mHeight = p.readInt();
        mTypeface = (Font) p.readValue();
    }

    /**
     * writeToParcel
     *
     * @param out
     * @param flags
     */
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(mMessage);
        out.writeString(mActionMessage);
        out.writeInt(mActionIcon);
        out.writeSequenceable(mToken);
        out.writeInt((int) mDuration);
        out.writeInt(mBtnTextColor);
        out.writeInt(mBackgroundColor);
        out.writeInt(mHeight);
        out.writeValue(mTypeface);
    }

    /**
     * describeContents
     *
     * @return 0
     */
    public int describeContents() {
        return 0;
    }

    /**
     * creates snack array
     */
    public static final Producer<Snack> CREATOR = new Producer<Snack>() {
        public Snack createFromParcel(Parcel in) {
            return new Snack(in);
        }

        public Snack[] newArray(int size) {
            return new Snack[size];
        }
    };

    /**
     * marshalling
     *
     * @param parcel
     * @return false
     */
    @Override
    public boolean marshalling(Parcel parcel) {
        return false;
    }

    /**
     * unmarshalling
     *
     * @param parcel
     * @return false
     */
    @Override
    public boolean unmarshalling(Parcel parcel) {
        return false;
    }
}
