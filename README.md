# SwipeStack

#### 项目介绍
- 项目名称: SwipeStack
- 所属系列：openharmony的第三方组件适配移植
- 功能： 可滑动视图堆栈
- 项目移植状态：完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：0.3.0


#### 效果演示
![输入图片说明](https://images.gitee.com/uploads/images/2021/0817/144309_498e5273_5356950.gif "3.gif")

#### 安装教程

1.在项目根目录下的build.gradle文件中，

 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```

2.在entry模块的build.gradle文件中，

 ```gradle
 dependencies {
     implementation('com.gitee.chinasoft_ohos:SwipeStack:1.0.0')
    ......  
 }
 ```

在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行

如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，

并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下


#### 使用说明


XML中添加控件:

```xml
    <link.fls.swipestack.SwipeDirectionalView
           ohos:id="$+id:swipeStack"
           ohos:height="match_parent"
           ohos:width="match_parent"/>
```

```java
    SwipeDirectionalView mSwipeStack;
 mSwipeStack = (SwipeDirectionalView) findComponentById(ResourceTable.Id_swipeStack);

        for (int i = 0; i < 5; i++) {
            mSwipeStack.addView(makeCard("Test " + mCount++), random.nextInt(20) - 10);
        }
       mSwipeStack.setSwipeStackListener(this);
```


#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

## 版本迭代
1.0.0

## 版权和许可信息
``` 
Copyright (C) 2016 Frederik Schweiger

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 ```
